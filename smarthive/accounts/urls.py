from rest_framework import routers
from django.urls import path
from django.conf.urls import include
from .api import RegisterAPI, LoginAPI, UserAPI
from knox import views as knox_views

# router = routers.DefaultRouter()
# router.register('auth/user', UserAPI, basename='User')
# router.register('auth/login', LoginAPI, basename='login')
# router.register('auth/register', RegisterAPI, basename='register')
# router.register('api/auth/logout', knox_views.LogoutView, basename='knox_logout')
# router.register('api/auth', include('knox.urls'), basename='knox_url')



# urlpatterns = router.urls

urlpatterns = [
    path('auth/user', UserAPI.as_view(), name='user'),
    path('auth/login', LoginAPI.as_view(), name='login'),
    path('auth/register', RegisterAPI.as_view(), name='register'),
    # path('', include(router.urls)),
    # path('api/auth', include('knox.urls')),
    # path('api/auth/logout', knox_views.LogoutView.as_view(), name='knox_logout')
    # path('add_sensor', AddSensor.as_view(),  name='add_scale'),
]


# urlpatterns = [
#     path('api/auth', include('knox.urls')),
#     path('api/auth/register', RegisterAPI.as_view(),  name='register'),
#     path('api/auth/login', LoginAPI.as_view(), name='login'),
#     path('api/auth/user', UserAPI.as_view(), name='user'),
#     path('api/auth/logout', knox_views.LogoutView.as_view(), name='knox_logout')
# ]
