from rest_framework import serializers
from .models import System


# System serializer
class SystemSerializer(serializers.ModelSerializer):

    class Meta:
        model = System
        fields = '__all__'
