from rest_framework import routers
from .views import SystemViewSet, GetSystems
from django.urls import path
from django.conf.urls import include

router = routers.DefaultRouter()
router.register('systems', SystemViewSet, basename='System')
router.register('get_systems', GetSystems, basename='System')

urlpatterns = [
    path('', include(router.urls)),

]
