from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from .serializers import SystemSerializer
from .models import System
from rest_framework.authentication import TokenAuthentication
from rest_framework import viewsets, permissions, generics, status
from rest_framework.decorators import action
from rest_framework.response import Response
import json


class SystemViewSet(viewsets.ModelViewSet):

    authentication_classes = TokenAuthentication

    permission_classes = [
        permissions.IsAuthenticated
    ]
    serializer_class = SystemSerializer

    def get_queryset(self):
        return self.request.user.system.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class GetSystems(viewsets.ModelViewSet):
    serializer_class = SystemSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get_queryset(self):
        user = self.request.user
        if user.is_superuser:
            return System.objects.all()
        return System.objects.filter(owner=user.id)


    # def get(self, request, *args, **kwargs):
    #     try:
    #         system = System.objects.filter(id=request.data["id"])
    #     except System.DoesNotExist:
    #         return Response(status=status.HTTP_404_NOT_FOUND)
    #
    #     return Response({
    #         'scale': SystemSerializer(system, many=True, context=self.get_serializer_context()).data
    #     })