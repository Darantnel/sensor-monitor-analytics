from django.db import models
# from django.contrib.auth.models import User
from accounts.models import Account as User

class System(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, default=True)
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField(null=True, auto_now=True)
    description = models.CharField(max_length=300)
