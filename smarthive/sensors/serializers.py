from rest_framework import serializers
from .models import Sensor, Value
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token


# Sensor serializer
class ValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = Value
        fields = ('sensor', 'measurement', 'metric', 'time')


# Sensor serializer
class SensorSerializer(serializers.ModelSerializer):
    # values = ValueSerializer(many=True, read_only=True)
    class Meta:
        model = Sensor
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username', 'password']
        # extra_kwargs = {'password': {'required': True, 'write_only': True}}

        # @staticmethod
        # def create(self, validate_data):
        #     user = User.objects.create_user(**validate_data)
        #     Token.objects.create(user=user)
        #     return user
