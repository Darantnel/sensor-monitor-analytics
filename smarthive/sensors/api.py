from .models import Sensor, Value
from rest_framework import viewsets, permissions, generics, status
from .serializers import SensorSerializer, ValueSerializer
from rest_framework.decorators import action
from rest_framework.response import Response
import json


# Sensor Viewset
class SensorViewSet(viewsets.ModelViewSet):

    permission_classes = [
        permissions.IsAuthenticated
    ]

    serializer_class = SensorSerializer

    def get_queryset(self):
        return self.request.user.sensor.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


# Add Sensor API
class AddSensorAPI(generics.GenericAPIView):
    serializer_class = SensorSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        sensor = serializer.save()
        return Response({
            'Sensor': SensorSerializer(sensor, context=self.get_serializer_context()).data
        })


class GetSensorsAPI(generics.GenericAPIView):
    serializer_class = SensorSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get(self, request, *args, **kwargs):
        if request.user:
            user = request.user
            if request.data.get("id"):
                sensor_id = request.data.get("id")
                try:
                    sensor = Sensor.objects.get(pk=sensor_id, owner=user.id)
                    return Response({'Sensors': SensorSerializer(sensor, context=self.get_serializer_context()).data})
                except Sensor.DoesNotExist:
                    return Response(status=status.HTTP_404_NOT_FOUND)
            else:
                try:
                    if user.is_superuser:
                        sensors = Sensor.objects.all()
                        return Response({'Sensors': SensorSerializer(sensors, context=self.get_serializer_context(), many=True).data})
                    sensors = Sensor.objects.get(owner=user.id)
                    return Response({'Sensors': SensorSerializer(sensors, context=self.get_serializer_context(), many=True).data})
                except Sensor.DoesNotExist:
                    return Response(status=status.HTTP_404_NOT_FOUND)


# Add Value API
class AddValueAPI(generics.GenericAPIView):
    serializer_class = ValueSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def post(self, request, *args, **kwargs):
        if request.user:
            user = request.user
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            if request.data.get("sensor"):
                sensor_id = request.data.get("sensor")
                try:
                    sensor = Sensor.objects.get(pk=sensor_id, owner=user.id)
                    if sensor:
                        print(sensor)
                        value = serializer.save()
                        return Response({'Value': ValueSerializer(value, context=self.get_serializer_context()).data})
                    else:
                        return Response(status=status.HTTP_417_EXPECTATION_FAILED)
                except Sensor.DoesNotExist:
                    return Response(status=status.HTTP_404_NOT_FOUND)

# Add Value API
class AddValuesAPI(generics.GenericAPIView):
    serializer_class = ValueSerializer
    queryset = []

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def post(self, request, *args, **kwargs):
        if request.user:
            ret_list = []
            user = request.user
            res = json.loads(request.body)
            for obj in res:
                serializer = self.get_serializer(data=obj)
                serializer.is_valid(raise_exception=True)
                if obj.get("sensor"):
                    sensor_id = obj.get("sensor")
                    try:
                        sensor = Sensor.objects.get(pk=sensor_id, owner=user.id)
                        if sensor:
                            value = serializer.save()
                            ret_list.append(ValueSerializer(value, context=self.get_serializer_context()).data)
                    except Sensor.DoesNotExist:
                        continue
            return Response({'Values': ret_list})



class GetValuesAPI(generics.GenericAPIView):
    serializer_class = ValueSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get(self, request, *args, **kwargs):
        if request.user:
            user = request.user
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            if request.data.get("sensor"):
                sensor_id = request.data.get("sensor")
                try:
                    if user.is_superuser:
                        sensor = Sensor.objects.get(pk=sensor_id)
                    else:
                        sensor = Sensor.objects.get(pk=sensor_id, owner=user.id)
                    if sensor:
                        values = Value.objects.filter(sensor_id=sensor_id)
                        return Response({'Values': ValueSerializer(values, context=self.get_serializer_context(), many=True).data})
                    else:
                        return Response(status=status.HTTP_417_EXPECTATION_FAILED)
                    return Response({'Values': ValueSerializer(values, context=self.get_serializer_context(), many=True).data})
                except Sensor.DoesNotExist:
                    return Response(status=status.HTTP_404_NOT_FOUND)