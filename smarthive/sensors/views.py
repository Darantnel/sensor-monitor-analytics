from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from .serializers import SensorSerializer, UserSerializer
from .models import Sensor
from accounts.models import Account as User
from rest_framework.authentication import TokenAuthentication
from .models import Sensor
from rest_framework import viewsets, permissions, generics, status
from .serializers import SensorSerializer
from rest_framework.decorators import action
from rest_framework.response import Response
import json
from django.http import HttpResponse


class SensorViewSet(viewsets.ModelViewSet):
    authentication_classes = TokenAuthentication

    permission_classes = [
        permissions.IsAuthenticated
    ]
    serializer_class = SensorSerializer

    def get_queryset(self):
        return self.request.user.sensor.all()

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class GetSensors(viewsets.ModelViewSet):
    serializer_class = SensorSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get_queryset(self):
        if self.request.GET.get("system_id"):
            system_id = self.request.GET.get("system_id")
            try:
                return Sensor.objects.filter(system=system_id)
            except Sensor.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            user = self.request.user
            try:
                if user.is_superuser:
                    return Sensor.objects.all()
                return Sensor.objects.filter(owner=user.id)
            except Sensor.DoesNotExist:
                return Response(status=status.HTTP_404_NOT_FOUND)


class AddSensor(viewsets.ModelViewSet):
    serializer_class = SensorSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        print(request)
        print("----------")
        print(serializer)
        sensor = serializer.save()
        return Response({
            'sensor': SensorSerializer(sensor, context=self.get_serializer_context()).data
        })


class UpdateSensor(viewsets.ModelViewSet):
    serializer_class = SensorSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def put(self, request, *args, **kwargs):
        try:
            Sensor.objects.filter(id=request.data["id"])
        except Sensor.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        sensor = serializer.save()
        return Response({
            'sensor': SensorSerializer(sensor, context=self.get_serializer_context()).data
        })


class DeleteSensor(viewsets.ModelViewSet):
    serializer_class = SensorSerializer

    permission_classes = [
        permissions.IsAuthenticated
    ]

    def delete(self, request, *args, **kwargs):
        try:
            Sensor.objects.filter(id=request.data["id"])
        except Sensor.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        sensor = serializer.delete()
        return Response({
            'sensor': SensorSerializer(sensor, context=self.get_serializer_context()).data
        })


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]

