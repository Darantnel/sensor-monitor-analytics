from rest_framework import routers
from .views import SensorViewSet, UserViewSet, GetSensors, AddSensor, UpdateSensor, DeleteSensor
from .api import AddSensorAPI, GetSensorsAPI, AddValueAPI, GetValuesAPI, AddValuesAPI
from django.urls import path
from django.conf.urls import include

# router = routers.DefaultRouter()
# router.register('sensors', SensorViewSet, basename='Sensor')
# router.register('get_sensors', GetSensors, basename='Sensor')
# router.register('add_sensors', AddSensor, basename='Sensor')
# router.register('update_sensors', UpdateSensor, basename='Sensor')
# router.register('delete_sensors', DeleteSensor, basename='Sensor')
# router.register('users', UserViewSet)

# urlpatterns = router.urls

urlpatterns = [
    # path('', include(router.urls)),
    path('add_sensor', AddSensorAPI.as_view(),  name='add_sensor'),
    path('get_sensor', GetSensorsAPI.as_view(),  name='get_sensor'),
    path('add_values', AddValuesAPI.as_view(),  name='add_values'),
    path('add_value', AddValueAPI.as_view(),  name='add_value'),
    path('get_value', GetValuesAPI.as_view(),  name='get_value'),
]
