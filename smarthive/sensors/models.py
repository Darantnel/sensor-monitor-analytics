from django.db import models
# from django.contrib.auth.models import User
from accounts.models import Account as User
from systems.models import System
# from djongo.models import ArrayField
from jsonfield import JSONField
from django.utils.deconstruct import deconstructible
from django import forms

# class ValueForm(forms.ModelForm):
#     class Meta:
#         model = Value
#         fields = (
#             'measurement', 'time', 'metric'
#         )


class Sensor(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, default=True)
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField(null=True, auto_now=True)
    description = models.CharField(max_length=300)
    system = models.ForeignKey(System, on_delete=models.CASCADE, default=True)
    # values = ArrayField(model_container=Value, model_form_class=ValueForm, default={})


class Value(models.Model):
    sensor = models.ForeignKey(Sensor, related_name='Sensor', on_delete=models.CASCADE, default=True)
    measurement = models.FloatField(null=False)
    time = models.DateTimeField(null=True, auto_now=False)
    metric = models.CharField(max_length=100)
